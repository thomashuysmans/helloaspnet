﻿CREATE TABLE [dbo].Products
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] VARCHAR(50) NOT NULL, 
    [Description] VARCHAR(150) NOT NULL, 
    [Price] INT NOT NULL
)
