﻿using HelloAspNet.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HelloAspNet.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Products()
        {

            var products = new List<Product>();

            //De connectionstring moet je nog aanpassen naar diegene dat naar jouw database wijst. Daar kan ik nog bij helpen of de papa ook.
            using (var connection = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TheShop;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"))
            {
                //Deze code zal je u nog herinnneren van andere oefeningen.
                var command = new SqlCommand("SELECT ID, Name, Description, Price FROM Products;", connection);

                connection.Open();
                var dataReader = command.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var product = new Product();
                        product.Id = dataReader.GetInt32(0);
                        product.Name = dataReader.GetString(1);
                        product.Description = dataReader.GetString(2);
                        product.Price = dataReader.GetInt32(3);
                        products.Add(product);
                    }
                }
                connection.Close();
            }


            return View(products);
        }
    }
}